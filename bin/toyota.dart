import 'car.dart';

class Toyota extends Car {
  @override
  void power() {
    print('Menggunakan tenaga hibrid : bensin dan baterai');
  }
}
