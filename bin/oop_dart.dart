import 'honda.dart';
import 'mahasiswa.dart';
import 'tesla.dart';
import 'toyota.dart';

void main(List<String> arguments) {
  //polymorphism

  // Toyota toyota = Toyota();
  // toyota.power();

  // Tesla tesla = Tesla();
  // tesla.power();
  // tampilkan();
  // print(tampilData());
  // print(luas_persegi(8, 5));
  // print(volume(3.4, 4.0, 7.0));

  //oop
  // Mahasiswa mahasiswa = Mahasiswa();
  // print(mahasiswa.entry_khs());
  // mahasiswa.nama = 'fahmi';
  // mahasiswa.nim = 'e1234';
  // mahasiswa.set_email('fahmi@polije.ac.id');

  // Honda honda = Honda();
  // honda.tahun_buat = 2022;
  // honda.setNomorRangka(12345);

  Toyota toyota = Toyota();
  toyota.power();
}

void tampilkan() {
  print('Menampilkan nilai 2');
}

String tampilData() {
  return 'Tampil Data return string';
}

int luas_persegi(int panjang, int lebar) {
  int luas = 0;
  luas = panjang * lebar; //proses pengolahan
  return luas;
}

double volume(double panjang, double lebar, double tinggi) {
  double hasil = 0.0;
  hasil = panjang * lebar * tinggi;
  return hasil;
}
