class Car {
  late int tahun_buat;
  late String nama_mobil;
  late String merk_mobil;
  late int _nomor_rangka;

  void setNomorRangka(int nomor) {
    this._nomor_rangka = nomor;
  }

  void power() {
    print('Bahan bakar menggunakan bensin');
  }
}
